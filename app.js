var http = require("http")  // similar to includes
var cool = require('cool-ascii-faces')
http.createServer(requestListener).listen(8081)
require('./weatherReader.js')('Maryville, Missouri');
function requestListener (req, res) {

   // Send the HTTP header 
   // HTTP Status: 200 : OK
   // Content Type: text/plain
   res.writeHead(200, {"Content-Type": "text/html"});
   res.write('<!DOCTYPE html>'+
   '<html>'+
   ' <head>'+
   ' <meta charset="utf-8" />'+
   ' <title>My Node.js page!</title>'+
   ' </head>'+ 
   ' <body>'+
   ' <h1>Welcome to my Virtual World!</h1>'+
   ' <h4>Happiness is sharing smile, So keep smiling....</h4>'+
   ' <h1>'+cool()+'</h1>'+
   ' <span>Note: Reload page to get more smiles.</span>'+
   ' </body>'+
   '</html>');
   
   // Prepare and send the res body 
   res.end()
}

console.log('Server running at http://127.0.0.1:8081/')
